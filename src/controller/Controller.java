package controller;

import java.io.File;
import java.io.FileReader;
import java.util.Iterator;
import java.util.Scanner;
import com.opencsv.CSVReader;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.VODaylyStatistic;
import model.vo.VOMovingViolations;
import view.MovingViolationsManagerView;

public class Controller 
{
	private MovingViolationsManagerView view;

	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */
	private IQueue<VODaylyStatistic> movingViolationsQueue;

	/**
	 * Pila donde se van a cargar los datos de los archivos
	 */
	private IStack<VOMovingViolations> movingViolationsStack;


	public Controller() 
	{
		view = new MovingViolationsManagerView();

		//TODO, inicializar la pila y la cola

		movingViolationsQueue=new Queue<VODaylyStatistic>();
		movingViolationsStack=new Stack<VOMovingViolations>();

	}

	public void run() 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin = false;

		while(!fin)
		{
			view.printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 1:
				this.loadMovingViolations();
				break;

			case 2:
				IQueue<VODaylyStatistic> dailyStatistics = this.getDailyStatistics();
				view.printDailyStatistics(dailyStatistics);
				break;

			case 3:
				view.printMensage("Ingrese el n�mero de infracciones a buscar");
				int n = sc.nextInt();

				IStack<VOMovingViolations> violations = this.nLastAccidents(n);
				view.printMovingViolations(violations);
				break;

			case 4:	
				fin=true;
				sc.close();
				break;
			}
		}
	}


	/**
	 * Carga los dos csv de infracciones 
	 */
	public void loadMovingViolations()
	{
		// TODO
		try
		{
			CSVReader csvReaderJan = new CSVReader(new FileReader("."+File.separator+"data"+File.separator+"Moving_Violations_Issued_in_January_2018_ordered.csv"));
			String[] fila=null;
			csvReaderJan.readNext();
			String fechaColaJan="";
			VODaylyStatistic diatmpJan = null;
			while ((fila=csvReaderJan.readNext()) != null) 
			{
				String objectId=fila[0];
				String violationDescrip=fila[15];
				String location= fila[2];
				String FineAMT=fila[8];
				String accidentIndicator=fila[12];
				String ticketIssueDate= fila[13].split("T")[0];

				//Crea la infraccion.
				VOMovingViolations infraccion=new VOMovingViolations(objectId, violationDescrip, location, ticketIssueDate);

				//La agrega a la pila
				movingViolationsStack.push(infraccion);

				//Crea el dia

				if(fechaColaJan.equalsIgnoreCase(ticketIssueDate))
				{
					diatmpJan.sumNumInfracciones();
					if(accidentIndicator.equalsIgnoreCase("Yes"))
					{
						diatmpJan.sumNumAccidentes();
					}
					diatmpJan.sumNumMultas(Integer.parseInt(FineAMT));
				}
				else
				{
					if(fechaColaJan=="")
					{
						diatmpJan=new VODaylyStatistic(ticketIssueDate);
						fechaColaJan=ticketIssueDate;
					}
					else
					{
						movingViolationsQueue.enqueue(diatmpJan);
						diatmpJan=new VODaylyStatistic(ticketIssueDate);
						fechaColaJan=ticketIssueDate;
					}
				}

			}
			movingViolationsQueue.enqueue(diatmpJan);

			csvReaderJan.close();

			// Carga el doc de la cola
			CSVReader csvReaderFeb = new CSVReader(new FileReader("."+File.separator+"data"+File.separator+"Moving_Violations_Issued_in_February_2018_ordered.csv"));
			String[] fila2=null;
			csvReaderFeb.readNext();
			String fechaColaFeb="";
			VODaylyStatistic diatmpFeb = null;
			while ((fila2=csvReaderFeb.readNext()) != null) 
			{
				String objectId=fila2[0];
				String violationDescrip=fila2[15];
				String location= fila2[2];
				String FineAMT=fila2[8];
				String accidentIndicator=fila2[12];
				String ticketIssueDate=fila2[13].split("T")[0];

				//Crea la infraccion.
				VOMovingViolations infraccion=new VOMovingViolations(objectId, violationDescrip, location, ticketIssueDate);

				//La agrega a la pila
				movingViolationsStack.push(infraccion);

				//Crea el dia
				if(fechaColaFeb.equalsIgnoreCase(ticketIssueDate))
				{
					diatmpFeb.sumNumInfracciones();
					if(accidentIndicator.equalsIgnoreCase("Yes"))
					{
						diatmpFeb.sumNumAccidentes();
					}
					diatmpFeb.sumNumMultas(Integer.parseInt(FineAMT));
				}
				else
				{
					if(fechaColaFeb=="")
					{
						diatmpFeb=new VODaylyStatistic(ticketIssueDate);
						fechaColaFeb=ticketIssueDate;
					}
					else
					{
						movingViolationsQueue.enqueue(diatmpFeb);
						diatmpFeb=new VODaylyStatistic(ticketIssueDate);
						fechaColaFeb=ticketIssueDate;
					}
				}
			}
			movingViolationsQueue.enqueue(diatmpFeb);

		}
		catch (Exception e) 
		{
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public IQueue <VODaylyStatistic> getDailyStatistics () 
	{
		// TODO
		return movingViolationsQueue;
	}

	public IStack <VOMovingViolations> nLastAccidents(int n) 
	{

		// TODO
		Stack<VOMovingViolations> ultimosAccidentes=new Stack<VOMovingViolations>();
		for (int i = 0; i < n; i++) 
		{
			VOMovingViolations tmp=movingViolationsStack.pop();
			ultimosAccidentes.push(tmp);
		}
		return ultimosAccidentes;

	}
}
