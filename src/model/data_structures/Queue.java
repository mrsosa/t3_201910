package model.data_structures;

import java.util.ArrayList;
import java.util.Iterator;

public class Queue<T> implements IQueue<T> 
{
	/**
	 * Arreglo dinamico que contiene los elementos de la cola 
	 */
	private ArrayList<T> cola;
	/**
	 * Constructor de la clase
	 */
    public Queue() 
	{
		// TODO Auto-generated constructor stub
    	cola=new ArrayList<T>();
	}
	
	@Override
	public Iterator<T> iterator() 
	{
		// TODO Auto-generated method stub
		return cola.iterator();
	}

	@Override
	public boolean isEmpty() 
	{
		// TODO Auto-generated method stub
		return cola.size()==0;
	}

	@Override
	public int size() 
	{
		// TODO Auto-generated method stub
		return cola.size();
	}

	@Override
	public void enqueue(T t) 
	{
		// TODO Auto-generated method stub
		cola.add(t);
	}

	@Override
	public T dequeue() 
	{
		// TODO Auto-generated method stub
		T rta = null;
		if(!isEmpty())
		{
			rta=cola.get(0);// Obtiene el elemento.
			cola.remove(0);// Remove elimina el primer elemento que entro y corre todos los demas a la izquierda.(FIFO)
		}
		return rta;
	}

}
