package model.vo;
/**
 * Representation of a Day of infractions
 */
public class VODaylyStatistic 
{
	//TODO
	/**
	 * fecha del d�a
	 */
	 String fecha;
	/**
	 * numero de accidentes del dia
	 */
	private int numAccidentes;
	/**
	 * numero de Infracciones;
	 */
	private int numInfracciones;
	/**
	 * Cantidad total de las multas del dia
	 */
	private int numMultas;
	/**
	 * Construye la estadistica del dia
	 * @param pFecha
	 */
	public VODaylyStatistic(String pFecha) 
	{
		fecha=pFecha;
		numAccidentes=0;
		numInfracciones=0;
		numMultas=0;
	}
	/**
	 * retorna la fecha;
	 * @return
	 */
	public String getFecha()
	{
		return fecha;
	}
	/**
	 * Retorna el numero de accidentes 
	 * @return
	 */
	public int getNumAccidentes()
	{
		return numAccidentes;
	}
	/**
	 * Retorna el numero de Infracciones
	 */
	public int getInfracciones()
	{
		return numInfracciones;
	}
	/**
	 * Retorna el numero de Infracciones
	 */
	public int getMultas()
	{
		return numMultas;
	}
	/**
	 * Suma uno al valor del numero de accidentes
	 */
	public void sumNumAccidentes()
	{
		numAccidentes+=1;
	}
	/**
	 * Reasigna el valor del numero de infracciones
	 */
	public void sumNumInfracciones()
	{
		numInfracciones+=1;
	}
	/**
	 * suma un valor por parametro al atributo multas	 
	 * */
	public void sumNumMultas(int pMultas)
	{
		numMultas+=pMultas;
	}
}
