package model.vo;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations 
{
	/**
	 * objectId del auto
	 */
	private String objectId;
	/**
	 * violationDescription del auto
	 */
	private String violationDescription;
	/**
	 * location del auto
	 */
	private String location;
	/**
	 * ticketIssueDate del auto
	 */
	private String ticketIssueDate;
	
	/**
	 * constructor
	 */
	public VOMovingViolations(String pObjectId,String pViolationDescription,String pLocation, String pTicketIssueDate)
	{ 
		objectId=pObjectId;
		violationDescription=pViolationDescription;
		location=pLocation;
		ticketIssueDate=pTicketIssueDate;
	}
	/**
	 * @return id - Identificador único de la infracción
	 */
	public int objectId() 
	{
		// TODO Auto-generated method stub
		return Integer.parseInt(objectId);
	}	

	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() {
		// TODO Auto-generated method stub
		return location;
	}
	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public String getTicketIssueDate() {
		// TODO Auto-generated method stub
		return ticketIssueDate;
	}

	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDescription() {
		// TODO Auto-generated method stub
		return violationDescription;
	}
}
